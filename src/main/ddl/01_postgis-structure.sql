---
-- #%L
-- T3 :: Installer
-- 
-- $Id$
-- $HeadURL$
-- %%
-- Copyright (C) 2010 - 2012 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---

-- ajout colonne geometry sur la table activite
SELECT AddGeometryColumn('activity', 'the_geom', 4326, 'POINT',2 );

-- ajout colonne geometry sur la table zoneet
SELECT AddGeometryColumn('zoneet','the_geom',4326,'POLYGON',2);

-- ajout colonne geomaetry sur la table zonefao
SELECT AddGeometryColumn('zonefao','the_geom', 4326,'MULTIPOLYGON',2);

-- ajout colonne geometry sur la table zoneee
SELECT AddGeometryColumn('zoneee','the_geom',4326,'MULTIPOLYGON', 2);

-- ajout colonne geometry sur la table zonecwp
SELECT AddGeometryColumn('zonecwp','the_geom',4326,'POLYGON', 2);

UPDATE activity SET the_geom=ST_SetSRID(ST_MakePoint(longitude,latitude), 4326);

-- ajout des index postgis (see http://forge.codelutin.com/issues/2289)
CREATE INDEX idx_activity_gist ON activity USING GIST (the_geom);
CREATE INDEX idx_zoneee_gist ON zoneee USING GIST (the_geom);
CREATE INDEX idx_zonecwp_gist ON zonecwp USING GIST (the_geom);
CREATE INDEX idx_zoneet_gist ON zoneet USING GIST (the_geom);
CREATE INDEX idx_zonefao_gist ON zonefao USING GIST (the_geom);

CREATE OR REPLACE function syncgg () returns trigger as '
BEGIN

  --  IF (TG_OP = ''UPDATE'') THEN ...

  IF (NEW.latitude IS NULL OR NEW.longitude IS NULL) THEN
    -- on ne calcule pas le point postgis si au moins une des
    -- coordonnees n est pas renseignee

    RAISE NOTICE ''No latitude or longitude, can not compute postgis field for activite % '', NEW.topiaId;
    return NEW;
  END IF;

  -- affectation du point
  NEW.the_geom := ST_SetSRID(ST_MakePoint(NEW.longitude,NEW.latitude), 4326);

  RAISE NOTICE ''Will compute the_geom for activite % - latitude % and longitude %'', NEW.topiaId, NEW.latitude, NEW.longitude;
  RAISE NOTICE ''Computed for activity % latitude % and longitude %, the_geom %'', NEW.topiaId, NEW.latitude, NEW.longitude, NEW.the_geom;

  RETURN NEW;
END
'
LANGUAGE 'plpgsql';

DROP TRIGGER IF EXISTS gg on activity;

CREATE TRIGGER gg BEFORE insert or update ON activity FOR EACH ROW EXECUTE PROCEDURE syncgg();
