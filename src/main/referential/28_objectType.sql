---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO objecttype(topiaid, topiaversion, topiacreatedate, code, rfmoscode, libelle) VALUES('fr.ird.t3.entities.reference.ObjectType#1454009635526#0.1', 0, '2016-01-28 00:00:00.000', 1,   'PAY DCP (artificiel) ancré', TRUE);
INSERT INTO objecttype(topiaid, topiaversion, topiacreatedate, code, rfmoscode, libelle) VALUES('fr.ird.t3.entities.reference.ObjectType#1454009635526#0.2', 0, '2016-01-28 00:00:00.000', 2,   'FAD DCP (artificiel) dérivant', TRUE);
INSERT INTO objecttype(topiaid, topiaversion, topiacreatedate, code, rfmoscode, libelle) VALUES('fr.ird.t3.entities.reference.ObjectType#1454009635526#0.3', 0, '2016-01-28 00:00:00.000', 3,   'LOG Objet non DCP (naturel ou artificiel) dérivant', TRUE);
INSERT INTO objecttype(topiaid, topiaversion, topiacreatedate, code, rfmoscode, libelle) VALUES('fr.ird.t3.entities.reference.ObjectType#1454009635526#0.4', 0, '2016-01-28 00:00:00.000', 4,   'SMT Mont sous-marin', FALSE);
INSERT INTO objecttype(topiaid, topiaversion, topiacreatedate, code, rfmoscode, libelle) VALUES('fr.ird.t3.entities.reference.ObjectType#1454009635526#0.5', 0, '2016-01-28 00:00:00.000', 5,   'WHS Requin-baleine', FALSE);
INSERT INTO objecttype(topiaid, topiaversion, topiacreatedate, code, rfmoscode, libelle) VALUES('fr.ird.t3.entities.reference.ObjectType#1454009635526#0.6', 0, '2016-01-28 00:00:00.000', 6,   'MAM Baleine', FALSE);
INSERT INTO objecttype(topiaid, topiaversion, topiacreatedate, code, rfmoscode, libelle) VALUES('fr.ird.t3.entities.reference.ObjectType#1454009635526#0.7', 0, '2016-01-28 00:00:00.000', 7,   'DOL Dauphin', FALSE);
INSERT INTO objecttype(topiaid, topiaversion, topiacreatedate, code, rfmoscode, libelle) VALUES('fr.ird.t3.entities.reference.ObjectType#1454009635526#0.8', 0, '2016-01-28 00:00:00.000', 9,   'Type inconnu', TRUE);
INSERT INTO objecttype(topiaid, topiaversion, topiacreatedate, code, rfmoscode, libelle) VALUES('fr.ird.t3.entities.reference.ObjectType#1454009635526#0.9', 0, '2016-01-28 00:00:00.000', 999,   'Aucun', TRUE);
UPDATE objecttype SET status = TRUE;
