---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO vesselsizecategory (topiaid, topiaversion, topiacreatedate, code, capacitylibelle, gaugelibelle, status) VALUES ('fr.ird.t3.entities.reference.VesselSizeCategory#1297580528730#0.1379381127148035', 1, '2011-02-13 08:02:05.622', 1, '20 tonnes', '- 95 TX', false);
INSERT INTO vesselsizecategory (topiaid, topiaversion, topiacreatedate, code, capacitylibelle, gaugelibelle, status) VALUES ('fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.8804897590207591', 1, '2011-02-13 08:02:05.653', 2, '40 tonnes', '95 - 189 TX', true);
INSERT INTO vesselsizecategory (topiaid, topiaversion, topiacreatedate, code, capacitylibelle, gaugelibelle, status) VALUES ('fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.4224620911289061', 1, '2011-02-13 08:02:05.653', 3, '90 tonnes', '190 - 299 TX', true);
INSERT INTO vesselsizecategory (topiaid, topiaversion, topiacreatedate, code, capacitylibelle, gaugelibelle, status) VALUES ('fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.9756782054555624', 1, '2011-02-13 08:02:05.653', 4, '200 - 400 tonnes', '300- 449 TX', true);
INSERT INTO vesselsizecategory (topiaid, topiaversion, topiacreatedate, code, capacitylibelle, gaugelibelle, status) VALUES ('fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.02105373822983636', 1, '2011-02-13 08:02:05.653', 5, '401 - 600 tonnes', '450 - 749 TX', true);
INSERT INTO vesselsizecategory (topiaid, topiaversion, topiacreatedate, code, capacitylibelle, gaugelibelle, status) VALUES ('fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.8372725392088513', 1, '2011-02-13 08:02:05.654', 6, '601 - 800 tonnes', '750 - 1249 TX', true);
INSERT INTO vesselsizecategory (topiaid, topiaversion, topiacreatedate, code, capacitylibelle, gaugelibelle, status) VALUES ('fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.4224481067346053', 1, '2011-02-13 08:02:05.654', 7, '801 - 1200 tonnes', '1250 - 2300 TX', true);
INSERT INTO vesselsizecategory (topiaid, topiaversion, topiacreatedate, code, capacitylibelle, gaugelibelle, status) VALUES ('fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.46323564513794135', 1, '2011-02-13 08:02:05.654', 8, '> 1200 tonnes', ' + 2300 TX', true);
INSERT INTO vesselsizecategory (topiaid, topiaversion, topiacreatedate, code, capacitylibelle, gaugelibelle, status) VALUES ('fr.ird.t3.entities.reference.VesselSizeCategory#1297580528731#0.1828610057804777', 1, '2011-02-13 08:02:05.654', 9, 'Inconnue', NULL, true);