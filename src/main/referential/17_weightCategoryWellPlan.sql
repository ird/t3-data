---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO PUBLIC.WEIGHTCATEGORYWELLPLAN(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, LIBELLE) VALUES
  ('fr.ird.t3.entities.reference.WeightCategoryWellPlan#1297580528984#0.54432733652597', 0, TIMESTAMP '2011-02-13 08:02:06.614', 1, '- 10 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryWellPlan#1297580528984#0.09778598759828461', 0, TIMESTAMP '2011-02-13 08:02:06.63', 2, '+ 10 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryWellPlan#1297580528984#0.6948877971437837', 0, TIMESTAMP '2011-02-13 08:02:06.63', 8, 'mélange'),
  ('fr.ird.t3.entities.reference.WeightCategoryWellPlan#1297580528984#0.07609400642577135', 0, TIMESTAMP '2011-02-13 08:02:06.63', 9, 'inconnu');

UPDATE WEIGHTCATEGORYWELLPLAN SET status = TRUE;
