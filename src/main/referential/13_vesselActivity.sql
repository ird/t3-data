---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO PUBLIC.VESSELACTIVITY(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, LIBELLE, LIBELLE8) VALUES
  ('fr.ird.t3.entities.reference.VesselActivity#1297580528706#0.4362868564425493', 0, TIMESTAMP '2011-02-13 08:02:05.573', 0, 'coup(s) nul(s)', 'coup -'),
  ('fr.ird.t3.entities.reference.VesselActivity#1297580528726#0.6977540500177304', 0, TIMESTAMP '2011-02-13 08:02:05.603', 1, 'coup(s) positif(s)', 'coup +'),
  ('fr.ird.t3.entities.reference.VesselActivity#1297580528726#0.08581829697697385', 0, TIMESTAMP '2011-02-13 08:02:05.603', 2, 'détail inconnu', 'coup ?'),
  ('fr.ird.t3.entities.reference.VesselActivity#1297580528726#0.3380796966617322', 0, TIMESTAMP '2011-02-13 08:02:05.603', 3, 'recherche', 'rech'),
  ('fr.ird.t3.entities.reference.VesselActivity#1297580528726#0.14402888755040266', 0, TIMESTAMP '2011-02-13 08:02:05.603', 4, 'route sans veille', 'route'),
  ('fr.ird.t3.entities.reference.VesselActivity#1297580528726#0.7929872511474158', 0, TIMESTAMP '2011-02-13 08:02:05.603', 5, 'pose de DCP', 'dcp +'),
  ('fr.ird.t3.entities.reference.VesselActivity#1297580528726#0.5686972731198322', 0, TIMESTAMP '2011-02-13 08:02:05.603', 6, 'relève de DCP', 'dcp -'),
  ('fr.ird.t3.entities.reference.VesselActivity#1297580528726#0.5194053553101096', 0, TIMESTAMP '2011-02-13 08:02:05.603', 7, 'avarie', 'avarie'),
  ('fr.ird.t3.entities.reference.VesselActivity#1297580528727#0.6506575462628384', 0, TIMESTAMP '2011-02-13 08:02:05.603', 8, 'à la cape', 'cape'),
  ('fr.ird.t3.entities.reference.VesselActivity#1297580528727#0.6614005050746831', 0, TIMESTAMP '2011-02-13 08:02:05.603', 9, 'à l''appât', 'appât'),
  ('fr.ird.t3.entities.reference.VesselActivity#1297580528727#0.7228961100082824', 0, TIMESTAMP '2011-02-13 08:02:05.611', 10, 'en attente', 'attente'),
  ('fr.ird.t3.entities.reference.VesselActivity#1297580528727#0.20495464867367508', 0, TIMESTAMP '2011-02-13 08:02:05.611', 11, 'transbordement en mer', 'transb'),
  ('fr.ird.t3.entities.reference.VesselActivity#1297580528727#0.9141276205827732', 0, TIMESTAMP '2011-02-13 08:02:05.611', 12, 'transb. depuis un senneur', 'tr_in'),
  ('fr.ird.t3.entities.reference.VesselActivity#1297580528727#0.7640536666612607', 0, TIMESTAMP '2011-02-13 08:02:05.611', 13, 'transb. vers un senneur', 'tr_out'),
  ('fr.ird.t3.entities.reference.VesselActivity#1297580528727#0.1901330834121593', 0, TIMESTAMP '2011-02-13 08:02:05.612', 14, 'chavire la poche', 'c_poche'),
  ('fr.ird.t3.entities.reference.VesselActivity#1297580528727#0.8461003230448697', 0, TIMESTAMP '2011-02-13 08:02:05.612', 15, 'au port', 'port');

-- Additionnal vessel activities types (new in AVDTH v34)
INSERT INTO PUBLIC.VESSELACTIVITY(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, LIBELLE, LIBELLE8) VALUES

  ('fr.ird.t3.entities.reference.VesselActivity#1392110089428#0.9451475668725775',0, TIMESTAMP '2014-02-11 08:00:00.000',19,'transb. vers un canneur','transb to bb'),
  ('fr.ird.t3.entities.reference.VesselActivity#1392110089428#0.8025708419723897',0, TIMESTAMP '2014-02-11 08:00:00.000',20,'transb. depuis un canneur','transb from bb'),
  ('fr.ird.t3.entities.reference.VesselActivity#1392110089428#0.13404661969920895',0, TIMESTAMP '2014-02-11 08:00:00.000',22,'dcp- sans opération balise','dcp- sans op balise'),
  ('fr.ird.t3.entities.reference.VesselActivity#1392110089428#0.5643843915085784',0, TIMESTAMP '2014-02-11 08:00:00.000',23,'dcp+ et balise+','dcp+ avec balise+'),
  ('fr.ird.t3.entities.reference.VesselActivity#1392110089428#0.5426335974515701',0, TIMESTAMP '2014-02-11 08:00:00.000',24,'dcp- et balise-','dcp- avec balise-'),
  ('fr.ird.t3.entities.reference.VesselActivity#1392110089428#0.07697575138187485',0, TIMESTAMP '2014-02-11 08:00:00.000',25,'balise+ sur dcp existant','balise+ sur dcp existant'),
  ('fr.ird.t3.entities.reference.VesselActivity#1392110089428#0.6436283172268336',0, TIMESTAMP '2014-02-11 08:00:00.000',26,'balise- sur dcp existant','balise- sur dcp existant'),
  ('fr.ird.t3.entities.reference.VesselActivity#1392110089428#0.24828707894111857',0, TIMESTAMP '2014-02-11 08:00:00.000',29,'visite de dcp équipé d''une balise','visite dcp avec balise'),
  ('fr.ird.t3.entities.reference.VesselActivity#1392110089428#0.9735238947568715',0, TIMESTAMP '2014-02-11 08:00:00.000',30,'visite de dcp sans balise','visite dcp sans balise'),
  ('fr.ird.t3.entities.reference.VesselActivity#1392110089428#0.21957811848347608',0, TIMESTAMP '2014-02-11 08:00:00.000',31,'renforcement d''un objet sans opération balise','renf dcp sans op balise'),
  ('fr.ird.t3.entities.reference.VesselActivity#1392110189681#0.8944656817639516',0, TIMESTAMP '2014-02-11 08:00:00.000',32,'renforcement d''un objet avec balise+','renf dcp avec balise+'),
  ('fr.ird.t3.entities.reference.VesselActivity#1392110189681#0.9352764437828279',0, TIMESTAMP '2014-02-11 08:00:00.000',33,'perte/fin transmission d''une balise','fin trsm'),
  ('fr.ird.t3.entities.reference.VesselActivity#1392110189681#0.9501370219047195',0, TIMESTAMP '2014-02-11 08:00:00.000',34,'Retrait d''une balise isolée','retrait balise isolée');

UPDATE vesselactivity SET status = TRUE;
