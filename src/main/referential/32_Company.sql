---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.1', 0, '2016-02-01 00:00:00.000', 1, 'Saupiquet', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.2', 0, '2016-02-01 00:00:00.000', 2, 'SAPMER', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.3', 0, '2016-02-01 00:00:00.000', 3, 'CFTO', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.4', 0, '2016-02-01 00:00:00.000', 4, 'IOSMS', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.5', 0, '2016-02-01 00:00:00.000', 5, 'TFC', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.6', 0, '2016-02-01 00:00:00.000', 6, 'ACF', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.7', 0, '2016-02-01 00:00:00.000', 7, 'CMB', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.8', 0, '2016-02-01 00:00:00.000', 8, 'Pêche et Froid', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.9', 0, '2016-02-01 00:00:00.000', 9, 'IAT', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.10', 0, '2016-02-01 00:00:00.000', 10, 'Le Bouter', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.11', 0, '2016-02-01 00:00:00.000', 11, 'ACF/CMB', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.12', 0, '2016-02-01 00:00:00.000', 12, 'COBRECAF', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.13', 0, '2016-02-01 00:00:00.000', 13, 'Charlot', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.14', 0, '2016-02-01 00:00:00.000', 14, 'Hartswater', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.15', 0, '2016-02-01 00:00:00.000', 15, 'Pevasa', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.16', 0, '2016-02-01 00:00:00.000', 16, 'Echebastar', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.17', 0, '2016-02-01 00:00:00.000', 17, 'Isabella Fishing', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.18', 0, '2016-02-01 00:00:00.000', 18, 'Atunsa', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.19', 0, '2016-02-01 00:00:00.000', 19, 'Albacora', TRUE);
INSERT INTO company(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.Company#1.20', 0, '2016-02-01 00:00:00.000', 20, 'Donwong', TRUE);
