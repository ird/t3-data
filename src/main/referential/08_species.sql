---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO PUBLIC.SPECIES(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, CODE3L, LIBELLE, SCIENTIFICLIBELLE, MEASUREDRATIO, LFLENGTHCLASSSTEP, THRESHOLDNUMBERLEVEL3FREESCHOOLTYPE, THRESHOLDNUMBERLEVEL3OBJECTSCHOOLTYPE) VALUES
  ('fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204', 0, TIMESTAMP '2011-02-13 08:02:06.254', 1, 'YFT', 'Albacore', 'Thunnus albacares', 1.0, 2, 150, 150),
  ('fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', 0, TIMESTAMP '2011-02-13 08:02:06.265', 2, 'SKJ', 'Listao', 'Katsuwonus pelamis', 0.1, 1, 150, 150),
  ('fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', 0, TIMESTAMP '2011-02-13 08:02:06.265', 3, 'BET', 'Patudo', 'Thunnus obesus', 1.0, 2, 25, 180),
  ('fr.ird.t3.entities.reference.Species#1297580528889#0.7537605030704515', 0, TIMESTAMP '2011-02-13 08:02:06.265', 4, 'ALB', 'Germon', 'Thunnus alalunga', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528889#0.4053316565120936', 0, TIMESTAMP '2011-02-13 08:02:06.265', 5, 'LTA', 'Thonine', 'Euthynnus alleteratus', 0.1, 1, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528889#0.5568385538955283', 0, TIMESTAMP '2011-02-13 08:02:06.265', 6, 'FRI', 'Auxide', 'Auxis thasard', 0.1, 1, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528889#0.7797586779656372', 0, TIMESTAMP '2011-02-13 08:02:06.265', 7, 'SHX', 'Requins', 'Lamnidae & Carcharidae', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528889#0.8288119155884791', 0, TIMESTAMP '2011-02-13 08:02:06.265', 8, 'DSC', 'Thonidés rejetés', NULL, 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528889#0.5683902688416475', 0, TIMESTAMP '2011-02-13 08:02:06.265', 9, 'MIX', 'Espèces mélangées de thonidés', NULL, 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528890#0.7235429225787087', 0, TIMESTAMP '2011-02-13 08:02:06.265', 10, 'KAW', 'Thonine orientale', 'Euthynnus affinis', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528890#0.5141201542305377', 0, TIMESTAMP '2011-02-13 08:02:06.265', 11, 'LOT', 'Thon mignon', 'Thunnus tonggol', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528890#0.15829471459979372', 0, TIMESTAMP '2011-02-13 08:02:06.265', 12, 'BLF', 'Thon à nageoires noires', 'Thunnus atlanticus', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528890#0.42115687710460803', 0, TIMESTAMP '2011-02-13 08:02:06.266', 13, 'BFT', 'Thon rouge', 'Thunnus thynnus thynnus', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528890#0.18430303815137705', 0, TIMESTAMP '2011-02-13 08:02:06.266', 14, 'SBF', 'Thon rouge du sud', 'Thunnus maccoyii', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528890#0.2032631853627409', 0, TIMESTAMP '2011-02-13 08:02:06.266', 15, 'BON', 'Bonite à dos rayé', 'Sarda sarda', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528890#0.37853239561355567', 0, TIMESTAMP '2011-02-13 08:02:06.266', 16, 'BIP', 'Bonito oriental', 'Sarda orientalis', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528890#0.4823294475226977', 0, TIMESTAMP '2011-02-13 08:02:06.267', 17, 'BLT', 'Bonitou', 'Auxis rochei', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528890#0.7195876762319068', 0, TIMESTAMP '2011-02-13 08:02:06.267', 18, 'FRZ', 'Auxides & Bonitou', 'Auxis spp.', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528890#0.05492166218055494', 0, TIMESTAMP '2011-02-13 08:02:06.267', 19, 'BOP', 'Palomette', 'Orcynopsis unicolor', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528890#0.9598281772783329', 0, TIMESTAMP '2011-02-13 08:02:06.267', 20, 'WAH', 'Thazard bâtard', 'Acanthocybium solandri', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528891#0.8321988986415713', 0, TIMESTAMP '2011-02-13 08:02:06.267', 21, 'SSM', 'Thazard atlantique, Maquereau espagnol', 'Scomberomorus maculatus', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528891#0.3702981130755375', 0, TIMESTAMP '2011-02-13 08:02:06.267', 22, 'KGM', 'Thazard serra', 'Scomberomorus cavalla', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528891#0.9291243253311353', 0, TIMESTAMP '2011-02-13 08:02:06.267', 23, 'MAW', 'Thazard blanc', 'Scomberomorus tritor', 1.0, 2, 300, 300);
INSERT INTO PUBLIC.SPECIES(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, CODE3L, LIBELLE, SCIENTIFICLIBELLE, MEASUREDRATIO, LFLENGTHCLASSSTEP, THRESHOLDNUMBERLEVEL3FREESCHOOLTYPE, THRESHOLDNUMBERLEVEL3OBJECTSCHOOLTYPE) VALUES
  ('fr.ird.t3.entities.reference.Species#1297580528891#0.6973713085931087', 0, TIMESTAMP '2011-02-13 08:02:06.267', 24, 'CER', 'Thazard franc', 'Scomberomorus regalis', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528891#0.06598964017890285', 0, TIMESTAMP '2011-02-13 08:02:06.267', 25, 'COM', 'Thazard rayé (Indo-Pacifique)', 'Scomberomorus commerson', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528891#0.277861084682149', 0, TIMESTAMP '2011-02-13 08:02:06.267', 26, 'GUT', 'Thazard ponctué (Indo-Pacifique)', 'Scomberomorus guttatus', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528891#0.8733458797590166', 0, TIMESTAMP '2011-02-13 08:02:06.267', 27, 'STS', 'Thazard cirrus', 'Scomberomorus lineolatus', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528891#0.07624446469154489', 0, TIMESTAMP '2011-02-13 08:02:06.267', 28, 'BRS', 'Thazard tacheté du sud', 'Scomberomorus brasiliensis', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528891#0.7255260110145089', 0, TIMESTAMP '2011-02-13 08:02:06.267', 29, 'KGX', 'Thazards non classés', 'Scomberomorus spp.', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528891#0.2626954826359922', 0, TIMESTAMP '2011-02-13 08:02:06.267', 30, 'SAI', 'Voilier Atlantique', 'Istiophorus albicans', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528891#0.42371878801807505', 0, TIMESTAMP '2011-02-13 08:02:06.267', 31, 'SFA', 'Voilier Indo-Pacifique', 'Istiophorus platypterus', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528892#0.1885405502623827', 0, TIMESTAMP '2011-02-13 08:02:06.267', 32, 'BLM', 'Makaire noir', 'Makaira indica', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528892#0.10329800580627058', 0, TIMESTAMP '2011-02-13 08:02:06.267', 33, 'BUM', 'Makaire bleu Atlantique', 'Makaira nigricans', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528892#0.8468938192204287', 0, TIMESTAMP '2011-02-13 08:02:06.267', 34, 'BLZ', 'Makaire bleu Indo-Pacifique', 'Makaira mazara', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528892#0.7387428235028691', 0, TIMESTAMP '2011-02-13 08:02:06.268', 35, 'WHM', 'Makaire blanc', 'Tetrapturus albidus', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528892#0.2565749169300484', 0, TIMESTAMP '2011-02-13 08:02:06.268', 36, 'MLS', 'Marlin rayé', 'Tetrapturus audax', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528892#0.2905388174926805', 0, TIMESTAMP '2011-02-13 08:02:06.268', 37, 'SPF', 'Makaire bécune & Marlin de Méditerranée', 'Tetrapurus pfluegeri & T. belone', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528892#0.5685612495908416', 0, TIMESTAMP '2011-02-13 08:02:06.268', 38, 'BIL', 'Poissons à rostre non classés', 'Istiophoridae spp.', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528892#0.9734051109859334', 0, TIMESTAMP '2011-02-13 08:02:06.268', 39, 'SWO', 'Espadon', 'Xiphias gladius', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528892#0.3497160306862599', 0, TIMESTAMP '2011-02-13 08:02:06.268', 40, 'BGT', 'Grands Thonidés', 'Thunini', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528892#0.8581060481567976', 0, TIMESTAMP '2011-02-13 08:02:06.268', 41, 'TUN', 'Thons et bonites non classés', 'Thunini & Sardini ', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528893#0.4145401332713441', 0, TIMESTAMP '2011-02-13 08:02:06.268', 43, 'RAV', 'Ravil', NULL, 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528893#0.8103466931799156', 0, TIMESTAMP '2011-02-13 08:02:06.268', 99, 'OTH', 'Espèces inconnues ou autres', NULL, 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528893#0.5318768995033861', 0, TIMESTAMP '2011-02-13 08:02:06.268', 701, 'xxx', 'Appât Sardine de France', '?', 1.0, 2, 300, 300);
INSERT INTO PUBLIC.SPECIES(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, CODE3L, LIBELLE, SCIENTIFICLIBELLE, MEASUREDRATIO, LFLENGTHCLASSSTEP, THRESHOLDNUMBERLEVEL3FREESCHOOLTYPE, THRESHOLDNUMBERLEVEL3OBJECTSCHOOLTYPE) VALUES
  ('fr.ird.t3.entities.reference.Species#1297580528893#0.4508776879595856', 0, TIMESTAMP '2011-02-13 08:02:06.268', 702, 'xxx', 'Appât Sardine ', '?', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528893#0.5816657386910375', 0, TIMESTAMP '2011-02-13 08:02:06.268', 703, 'xxx', 'Appât Chinchard', '?', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528893#0.16090069597553502', 0, TIMESTAMP '2011-02-13 08:02:06.268', 704, 'xxx', 'Appât Pyronos', '?', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528893#0.5305774036220411', 0, TIMESTAMP '2011-02-13 08:02:06.268', 705, 'xxx', 'Appât Anchois', '?', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528893#0.2958994878325616', 0, TIMESTAMP '2011-02-13 08:02:06.268', 706, 'xxx', 'Appât Sardine', '?', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528893#0.9539593096416237', 0, TIMESTAMP '2011-02-13 08:02:06.268', 707, 'xxx', 'Appât Bogas', '?', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528893#0.691484221909629', 0, TIMESTAMP '2011-02-13 08:02:06.268', 708, 'xxx', 'Appât Sardine', '?', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528893#0.866664221472501', 0, TIMESTAMP '2011-02-13 08:02:06.268', 709, 'xxx', 'Appât Inconnu', '?', 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528893#0.18704485470829113', 0, TIMESTAMP '2011-02-13 08:02:06.268', 801, 'Dyf', 'Albacore rejeté', NULL, 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528894#0.6090603164066191', 0, TIMESTAMP '2011-02-13 08:02:06.268', 802, 'Dsk', 'Listao rejeté', NULL, 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528894#0.5515829087627862', 0, TIMESTAMP '2011-02-13 08:02:06.268', 803, 'Dbe', 'Patudo rejeté', NULL, 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528894#0.6319671377494842', 0, TIMESTAMP '2011-02-13 08:02:06.269', 804, 'Dal', 'Germon rejeté', NULL, 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528894#0.8908576247454943', 0, TIMESTAMP '2011-02-13 08:02:06.269', 805, 'Dlt', 'Thonine rejeté', NULL, 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528894#0.5783125750061949', 0, TIMESTAMP '2011-02-13 08:02:06.269', 806, 'Dfr', 'Auxide rejeté', NULL, 1.0, 2, 25, 25),
  ('fr.ird.t3.entities.reference.Species#1297580528894#0.15942007915259582', 0, TIMESTAMP '2011-02-13 08:02:06.269', 811, 'Dlo', 'Thon mignon rejeté', NULL, 1.0, 2, 25, 25);

-- Add faoid (since version 1.3.1)
UPDATE species SET faoid = 'YFT' WHERE code = 1;
UPDATE species SET faoid = 'SKJ' WHERE code = 2;
UPDATE species SET faoid = 'BET' WHERE code = 3;
UPDATE species SET faoid = 'ALB' WHERE code = 4;
UPDATE species SET faoid = 'LTA' WHERE code = 5;
UPDATE species SET faoid = 'FRI' WHERE code = 6;
UPDATE species SET faoid = 'MSK' WHERE code = 7;

UPDATE species SET faoid = 'KAW' WHERE code = 10;
UPDATE species SET faoid = 'LOT' WHERE code = 11;
UPDATE species SET faoid = 'BLF' WHERE code = 12;
UPDATE species SET faoid = 'BFT' WHERE code = 13;
UPDATE species SET faoid = 'SBF' WHERE code = 14;
UPDATE species SET faoid = 'BON' WHERE code = 15;

UPDATE species SET faoid = 'BLT' WHERE code = 17;

UPDATE species SET faoid = 'MAZ' WHERE code = 29;
UPDATE species SET faoid = 'SAI' WHERE code = 30;
UPDATE species SET faoid = 'SFA' WHERE code = 31;
UPDATE species SET faoid = 'BLM' WHERE code = 32;
UPDATE species SET faoid = 'BUM' WHERE code = 33;

UPDATE species SET faoid = 'WHM' WHERE code = 35;
UPDATE species SET faoid = 'MLS' WHERE code = 36;
UPDATE species SET faoid = 'SPF' WHERE code = 37;
UPDATE species SET faoid = 'BIL' WHERE code = 38;
UPDATE species SET faoid = 'SWO' WHERE code = 39;

-- Add more faoid (since version 1.4)
UPDATE species SET faoid = 'BIP' WHERE code = 16;
UPDATE species SET faoid = 'FRZ' WHERE code = 18;
UPDATE species SET faoid = 'BOP' WHERE code = 19;
UPDATE species SET faoid = 'WAH' WHERE code = 20;
UPDATE species SET faoid = 'SSM' WHERE code = 21;
UPDATE species SET faoid = 'KGM' WHERE code = 22;
UPDATE species SET faoid = 'MAW' WHERE code = 23;
UPDATE species SET faoid = 'CER' WHERE code = 24;
UPDATE species SET faoid = 'COM' WHERE code = 25;
UPDATE species SET faoid = 'GUT' WHERE code = 26;
UPDATE species SET faoid = 'STS' WHERE code = 27;
UPDATE species SET faoid = 'BRS' WHERE code = 28;
UPDATE species SET faoid = '1BUM' WHERE code = 34;
UPDATE species SET faoid = '1RAV' WHERE code = 43;

-- Additionnal species
INSERT INTO species (topiaid, topiaversion, topiacreatedate, code, code3l, libelle, scientificlibelle, measuredratio, lflengthclassstep, thresholdnumberlevel3freeschooltype, thresholdnumberlevel3objectschooltype, faoid, wormsid) VALUES

  ('fr.ird.t3.entities.reference.Species#1447133411737#0.43737406793204747', 0, '2015-11-10 09:41:00.000', 10690, 'TRI', 'Balistes nca', 'Balistidae', 1, 2, 300, 300, 'TRI', NULL),
  ('fr.ird.t3.entities.reference.Species#1447133411737#0.8837378532761647', 0, '2015-11-10 09:41:00.000', 3033, 'DOX', 'Coryphènes nca', 'Coryphaenidae', 1, 2, 300, 300, 'DOX', NULL),
  ('fr.ird.t3.entities.reference.Species#1447133411737#0.17658886596164214', 0, '2015-11-10 09:41:00.000', 9491, 'RRU', 'Comète saumon', 'Elagatis bipinnulata', 1, 2, 300, 300, 'RRU', NULL);

UPDATE species SET status = TRUE;

INSERT INTO species (topiaid, topiaversion, topiacreatedate, code, code3l, libelle, scientificlibelle, measuredratio, lflengthclassstep, thresholdnumberlevel3freeschooltype, thresholdnumberlevel3objectschooltype, faoid, wormsid, status) VALUES 
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.3', 0, '2016-02-17 17:02:00.000', 850, 'DTRI', 'Rejet/discard Balistes nca', 'Rejet/discard Balistidae', 1, 1, 25, 25, NULL, NULL, TRUE),
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.4', 0, '2016-02-17 17:02:00.000', 844, 'DDOX', 'Rejet/discard Coryphènes nca', 'Rejet/discard Coryphaenidae', 1, 1, 25, 25, NULL, NULL, TRUE),
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.5', 0, '2016-02-17 17:02:00.000', 847, 'DRRU', 'Rejet/discard Comète saumon', 'Rejet/discard Elagatis bipinnulata', 1, 1, 25, 25, NULL, NULL, TRUE),
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.6', 0, '2016-02-17 17:02:00.000', 820, 'DWAH', 'Rejet/discard Thazard bâtard', 'Rejet/discard Acanthocybium solandri', 1, 1, 25, 25, NULL, NULL, TRUE),
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.7', 0, '2016-02-17 17:02:00.000', 809, 'DMIX', 'Rejet/discard mélange/mix', 'Rejet/discard mélange/mix', 1, 1, 25, 25, NULL, NULL, TRUE),
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.8', 0, '2016-02-17 17:02:00.000', 807, 'DSHX', 'Rejet/discard Requins', 'Rejet/discard Lamnidae et Carcharhinidae', 1, 1, 25, 25, NULL, NULL, TRUE),
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.9', 0, '2016-02-17 17:02:00.000', 838, 'DBIL', 'Rejet/discard Poissons à rostre non classés', 'Rejet/discard Istiophoridae spp.', 1, 1, 25, 25, NULL, NULL, TRUE),
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.10', 0, '2016-02-17 17:02:00.000', 899, 'DOTH', 'Rejet/discard Espèces inconnues ou autres', 'Rejet/discard Espèces inconnues ou autres', 1, 1, 25, 25, NULL, NULL, TRUE),
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.11', 0, '2016-02-17 17:02:00.000', 49, 'TRE', 'Chinchards, carangues nca', 'Caranx spp', 1, 1, 25, 25, NULL, NULL, TRUE),
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.12', 0, '2016-02-17 17:02:00.000', 50, 'TRI', 'Balistes nca', 'Balistidae', 1, 1, 25, 25, NULL, NULL, TRUE),
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.13', 0, '2016-02-17 17:02:00.000', 56, 'RUB', 'Carangue coubali', 'Caranx crysos', 1, 1, 25, 25, NULL, NULL, TRUE),
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.14', 0, '2016-02-17 17:02:00.000', 830, 'DSAI', 'Rejet/discard Voilier Atlantique', 'Rejet/discard Istiophorus albicans', 1, 1, 25, 25, NULL, NULL, TRUE),
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.15', 0, '2016-02-17 17:02:00.000', 833, 'DBUM', 'Rejet/discard Makaire bleu Atlantique', 'Rejet/discard Makaira nigricans', 1, 1, 25, 25, NULL, NULL, TRUE),
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.16', 0, '2016-02-17 17:02:00.000', 849, 'DTRE', 'Rejet/discard Chinchards, carangues nca', 'Rejet/discard Caranx spp', 1, 1, 25, 25, NULL, NULL, TRUE);
INSERT INTO species (topiaid, topiaversion, topiacreatedate, code, code3l, libelle, scientificlibelle, measuredratio, lflengthclassstep, thresholdnumberlevel3freeschooltype, thresholdnumberlevel3objectschooltype, faoid, wormsid, status) VALUES 
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.17', 0, '2016-02-17 17:02:00.000', 846, 'DOCS', 'Rejet/discard Requin océanique', 'Rejet/discard Carcharhinus longimanus', 1, 1, 25, 25, NULL, NULL, TRUE),
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.18', 0, '2016-02-17 17:02:00.000', 47, 'RRU', 'Comète saumon', 'Elagatis bipinnulata', 1, 1, 25, 25, NULL, NULL, TRUE),
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.19', 0, '2016-02-17 17:02:00.000', 848, 'DSRX', 'Rejet/discard Raies, pastenagues, mantes nca', 'Rejet/discard Rajiformes', 1, 1, 25, 25, NULL, NULL, TRUE),
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.20', 0, '2016-02-17 17:02:00.000', 853, 'DFAL', 'Rejet/discard Requin soyeux', 'Rejet/discard Carcharhinus falciformis', 1, 1, 25, 25, NULL, NULL, TRUE),
  ('fr.ird.t3.entities.reference.Species#1455728697973#0.21', 0, '2016-02-17 17:02:00.000', 44, 'DOX', 'Coryphènes nca', 'Coryphaenidae', 1, 1, 25, 25, NULL, NULL, TRUE);