---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO PUBLIC.SAMPLEQUALITY(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, LIBELLE) VALUES
  ('fr.ird.t3.entities.reference.SampleQuality#1297580528939#0.09608239651708184', 0, TIMESTAMP '2011-02-13 08:02:06.384', 1, 'Super '),
  ('fr.ird.t3.entities.reference.SampleQuality#1297580528939#0.9758430051000159', 0, TIMESTAMP '2011-02-13 08:02:06.393', 2, 'Biologie - Taille'),
  ('fr.ird.t3.entities.reference.SampleQuality#1297580528939#0.48671868626923986', 0, TIMESTAMP '2011-02-13 08:02:06.393', 3, 'Biologie '),
  ('fr.ird.t3.entities.reference.SampleQuality#1297580528939#0.8399436548327087', 0, TIMESTAMP '2011-02-13 08:02:06.393', 4, 'Mauvais'),
  ('fr.ird.t3.entities.reference.SampleQuality#1297580528939#0.8410126395623535', 0, TIMESTAMP '2011-02-13 08:02:06.393', 9, 'Indéterminé');

UPDATE samplequality SET status = TRUE;
