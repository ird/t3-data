---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO PUBLIC.WELLDESTINY(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, LIBELLE) VALUES
  ('fr.ird.t3.entities.reference.WellDestiny#1297580528980#0.5475153020829012', 0, TIMESTAMP '2011-02-13 08:02:06.578', 1, 'Usines'),
  ('fr.ird.t3.entities.reference.WellDestiny#1297580528981#0.13719913518730464', 0, TIMESTAMP '2011-02-13 08:02:06.585', 2, 'Congélateurs'),
  ('fr.ird.t3.entities.reference.WellDestiny#1297580528981#0.7719599817686854', 0, TIMESTAMP '2011-02-13 08:02:06.611', 9, 'Indéterminé');

UPDATE WELLDESTINY SET status = TRUE;
