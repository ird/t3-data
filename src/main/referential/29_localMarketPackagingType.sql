---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO localmarketpackagingtype(topiaid, topiaversion, topiacreatedate, code, libelle) VALUES('fr.ird.t3.entities.reference.LocalMarketPackaging#1454009635523#0.1', 0, '2016-01-28 00:00:00.000', 1, 'Vrac pesé ou non (multispécifique) (sondage)');
INSERT INTO localmarketpackagingtype(topiaid, topiaversion, topiacreatedate, code, libelle) VALUES('fr.ird.t3.entities.reference.LocalMarketPackaging#1454009635523#0.2', 0, '2016-01-28 00:00:00.000', 2, 'Pesée (monospécifique) (pas de sondage)');
INSERT INTO localmarketpackagingtype(topiaid, topiaversion, topiacreatedate, code, libelle) VALUES('fr.ird.t3.entities.reference.LocalMarketPackaging#1454009635523#0.3', 0, '2016-01-28 00:00:00.000', 3, 'Espèce unité (monospécifique) (pas de sondage)');
INSERT INTO localmarketpackagingtype(topiaid, topiaversion, topiacreatedate, code, libelle) VALUES('fr.ird.t3.entities.reference.LocalMarketPackaging#1454009635523#0.4', 0, '2016-01-28 00:00:00.000', 4, 'Paquet non pesé (monospécifique) (pas de sondage)');
UPDATE localmarketpackagingtype SET status = TRUE;
