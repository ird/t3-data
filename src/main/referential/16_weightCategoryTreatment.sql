---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO PUBLIC.WEIGHTCATEGORYTREATMENT(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES
  ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830500#0.6310700924396421', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', TIMESTAMP '2011-06-24 16:30:30.5', '-10 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830505#0.988038536400381', 0, 10, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', TIMESTAMP '2011-06-24 16:30:30.505', '+10 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830505#0.988038536400382', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', TIMESTAMP '2011-06-24 16:30:30.505', 'Indéfini'),
  ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830505#0.44386731073429053', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', TIMESTAMP '2011-06-24 16:30:30.505', '-10 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830506#0.5237318856431237', 0, 10, 30, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', TIMESTAMP '2011-06-24 16:30:30.506', '10-30 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830506#0.5499777108807192', 0, 30, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', TIMESTAMP '2011-06-24 16:30:30.506', '+30 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830506#0.5499777108807193', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', TIMESTAMP '2011-06-24 16:30:30.506', 'Indéfini'),
  ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830506#0.4807760142660965', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', TIMESTAMP '2011-06-24 16:30:30.506', '-10 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830507#0.14752652961660062', 0, 10, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', TIMESTAMP '2011-06-24 16:30:30.507', '+10 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830507#0.14752652961660063', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', TIMESTAMP '2011-06-24 16:30:30.507', 'Indéfini'),
  ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830507#0.6434841998774135', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', TIMESTAMP '2011-06-24 16:30:30.507', '-10 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830508#0.8969775036221255', 0, 10, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', TIMESTAMP '2011-06-24 16:30:30.508', '+10 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830508#0.8969775036221256', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', TIMESTAMP '2011-06-24 16:30:30.508', 'Indéfini');

-- ocean atlantique (missing BI)
INSERT INTO weightcategorytreatment(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830509#0.44386731073429053', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.505', '-10 kg');
INSERT INTO weightcategorytreatment(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830509#0.5237318856431237', 0, 10, 30, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.506', '10-30 kg');
INSERT INTO weightcategorytreatment(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830509#0.5499777108807192', 0, 30, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.506', '+30 kg');
INSERT INTO weightcategorytreatment(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830509#0.5499777108807193', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.506', 'Indéfini');

-- ocean indien (missing BI)
INSERT INTO weightcategorytreatment(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830510#0.6434841998774145', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.507', '-10 kg');
INSERT INTO weightcategorytreatment(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830510#0.8969775036221246', 0, 10, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.508', '+10 kg');
INSERT INTO weightcategorytreatment(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#1308925830510#0.8969775036221247', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.508', 'Indéfini');

-- ocean pacifique (missing BO/BL/BI)
INSERT INTO weightcategorytreatment(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830506#0.4807760142660965', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', '2011-06-24 16:30:30.506', '-10 kg');
INSERT INTO weightcategorytreatment(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830507#0.14752652961660062', 0, 10, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', '2011-06-24 16:30:30.507', '+10 kg');
INSERT INTO weightcategorytreatment(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830507#0.14752652961660063', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', '2011-06-24 16:30:30.507', 'Indéfini');
INSERT INTO weightcategorytreatment(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830507#0.6434841998774135', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', '2011-06-24 16:30:30.507', '-10 kg');
INSERT INTO weightcategorytreatment(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830508#0.8969775036221255', 0, 10, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', '2011-06-24 16:30:30.508', '+10 kg');
INSERT INTO weightcategorytreatment(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830508#0.8969775036221256', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', '2011-06-24 16:30:30.508', 'Indéfini');
INSERT INTO weightcategorytreatment(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830510#0.6434841998774145', 0, 0, 10, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.507', '-10 kg');
INSERT INTO weightcategorytreatment(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830510#0.8969775036221246', 0, 10, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.508', '+10 kg');
INSERT INTO weightcategorytreatment(TOPIAID, TOPIAVERSION, MIN, MAX, OCEAN, SCHOOLTYPE, TOPIACREATEDATE, LIBELLE) VALUES ('fr.ird.t3.entities.reference.WeightCategoryTreatment#2308925830510#0.8969775036221247', 0, NULL, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', '2011-06-24 16:30:30.508', 'Indéfini');

UPDATE weightcategorytreatment SET status = TRUE;
