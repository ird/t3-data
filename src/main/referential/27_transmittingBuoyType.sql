---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO transmittingbuoytype(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.TransmittingBuoyType#1454009635525#0.1', 0, '2016-01-28 00:00:00.000', 1, 'Radiogoniomètre', TRUE);
INSERT INTO transmittingbuoytype(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.TransmittingBuoyType#1454009635525#0.2', 0, '2016-01-28 00:00:00.000', 2, 'Radiogonio + GPS', FALSE);
INSERT INTO transmittingbuoytype(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.TransmittingBuoyType#1454009635525#0.3', 0, '2016-01-28 00:00:00.000', 3, 'GPS SHERPE', FALSE);
INSERT INTO transmittingbuoytype(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.TransmittingBuoyType#1454009635525#0.4', 0, '2016-01-28 00:00:00.000', 4, 'Satellite + échosondeur indéterminé ', TRUE);
INSERT INTO transmittingbuoytype(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.TransmittingBuoyType#1454009635525#0.5', 0, '2016-01-28 00:00:00.000', 5, 'Satellite sans échosondeur', TRUE);
INSERT INTO transmittingbuoytype(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.TransmittingBuoyType#1454009635525#0.6', 0, '2016-01-28 00:00:00.000', 6, 'Satellite + Sonar', FALSE);
INSERT INTO transmittingbuoytype(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.TransmittingBuoyType#1454009635525#0.7', 0, '2016-01-28 00:00:00.000', 7, 'Satellite + échosondeur Zunibal', FALSE);
INSERT INTO transmittingbuoytype(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.TransmittingBuoyType#1454009635525#0.8', 0, '2016-01-28 00:00:00.000', 8, 'Satellite + échosondeur Satlink', FALSE);
INSERT INTO transmittingbuoytype(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.TransmittingBuoyType#1454009635525#0.9', 0, '2016-01-28 00:00:00.000', 9, 'Satellite + échosondeur Nautical', FALSE);
INSERT INTO transmittingbuoytype(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.TransmittingBuoyType#1454009635525#0.10', 0, '2016-01-28 00:00:00.000', 10, 'Satellite + échosondeur autre', FALSE);
INSERT INTO transmittingbuoytype(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.TransmittingBuoyType#1454009635525#0.11', 0, '2016-01-28 00:00:00.000', 98, 'Balise inconnue ou indéterminée', TRUE);
INSERT INTO transmittingbuoytype(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.TransmittingBuoyType#1454009635525#0.12', 0, '2016-01-28 00:00:00.000', 99, 'Type à  préciser', TRUE);
INSERT INTO transmittingbuoytype(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.TransmittingBuoyType#1454009635525#0.13', 0, '2016-01-28 00:00:00.000', 999, 'Aucune balise', TRUE);
