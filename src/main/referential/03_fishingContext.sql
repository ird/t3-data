---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO PUBLIC.FISHINGCONTEXT(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, REDUCEDCODE, LIBELLE, SCHOOLTYPE) VALUES
  ('fr.ird.t3.entities.reference.FishingContext#1297580528911#0.4246112211307743', 0, TIMESTAMP '2011-02-13 08:02:06.323', 0, 6, 'INCONNU', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528911#0.9904132443050981', 0, TIMESTAMP '2011-02-13 08:02:06.332', 10, 1, 'PECHE SUR HAUT-FOND  (  guyot )', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528911#0.7383490364467539', 0, TIMESTAMP '2011-02-13 08:02:06.332', 11, 6, 'PECHE SUR FRONT THERMIQUE', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528911#0.8817537639730286', 0, TIMESTAMP '2011-02-13 08:02:06.332', 12, 6, 'PECHE SUR LIGNE DE MAREE ', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528911#0.9946652863045611', 0, TIMESTAMP '2011-02-13 08:02:06.332', 13, 6, 'PECHE AUX ACCORES', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528911#0.6711970193132987', 0, TIMESTAMP '2011-02-13 08:02:06.332', 14, 6, 'PECHE DE NUIT', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528911#0.17265159608712655', 0, TIMESTAMP '2011-02-13 08:02:06.332', 20, 1, 'EPAVE ( sans précision )', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528912#0.31239575403600073', 0, TIMESTAMP '2011-02-13 08:02:06.332', 21, 1, 'EPAVE NATURELLE ( tas de paille, bille de bois .... )', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528912#0.03380014794705033', 0, TIMESTAMP '2011-02-13 08:02:06.332', 22, 1, 'EPAVE  NATURELLE BALISEE', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528912#0.06283208446807265', 0, TIMESTAMP '2011-02-13 08:02:06.332', 23, 1, 'EPAVE ARTIFICIELLE  ( caisse , bouée, cordage .... )', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528912#0.15646708890518535', 0, TIMESTAMP '2011-02-13 08:02:06.332', 24, 1, 'EPAVE ARTIFICIELLE  BALISEE  ( radeau )', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528912#0.78783978572659', 0, TIMESTAMP '2011-02-13 08:02:06.332', 25, 1, 'EPAVE ANCREE  ( D.C.P )', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528912#0.3488551095740673', 0, TIMESTAMP '2011-02-13 08:02:06.332', 26, 5, 'POISSON SOUS LE THONIER OU LE SKIFF ', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528912#0.8620243624657786', 0, TIMESTAMP '2011-02-13 08:02:06.332', 27, 5, 'PECHE AVEC UN CANNEUR', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528912#0.720298849740238', 0, TIMESTAMP '2011-02-13 08:02:06.333', 28, 5, 'PECHE AVEC SUPPLY OU AUTRE BATEAU ( autre que canneur )', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528912#0.7910741195697316', 0, TIMESTAMP '2011-02-13 08:02:06.333', 29, 2, 'PECHE A LA SENNE AVEC APPATS ', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528912#0.6890836302874168', 0, TIMESTAMP '2011-02-13 08:02:06.334', 30, 2, 'OISEAUX  ( sans précision )', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473');
INSERT INTO PUBLIC.FISHINGCONTEXT(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, REDUCEDCODE, LIBELLE, SCHOOLTYPE) VALUES
  ('fr.ird.t3.entities.reference.FishingContext#1297580528912#0.5393261078217007', 0, TIMESTAMP '2011-02-13 08:02:06.334', 31, 2, 'CONCENTRATION D''OISEAUX', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528913#0.41984602429451834', 0, TIMESTAMP '2011-02-13 08:02:06.334', 32, 2, 'DETECTION DU BANC AU SONAR', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528913#0.03588018572302043', 0, TIMESTAMP '2011-02-13 08:02:06.334', 33, 2, 'DETECTION DU BANC AU SONDEUR', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528913#0.1860794016968239', 0, TIMESTAMP '2011-02-13 08:02:06.334', 34, 2, 'DETECTION DU BANC PAR AVION', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528913#0.9298984160080317', 0, TIMESTAMP '2011-02-13 08:02:06.334', 35, 2, 'DETECTION DU BANC PAR HELICOPTERE', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528913#0.40531710382943564', 0, TIMESTAMP '2011-02-13 08:02:06.334', 36, 2, 'DETECTION AU RADAR', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528913#0.3749593145196174', 0, TIMESTAMP '2011-02-13 08:02:06.334', 37, 2, 'BANC SIGNALE PAR UN AUTRE BATEAU', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528913#0.19872529379401993', 0, TIMESTAMP '2011-02-13 08:02:06.334', 38, 2, 'ZONE DE PECHE SIGNALEE PAR SYSTEME EXTERIEUR ( carte satellite  ....)', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528913#0.20955887687821384', 0, TIMESTAMP '2011-02-13 08:02:06.334', 40, 2, 'GLEURE', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528913#0.276274555139412', 0, TIMESTAMP '2011-02-13 08:02:06.334', 41, 2, 'PETITS PELAGIQUES ( anchois , sardines .... )', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528913#0.14829157423000205', 0, TIMESTAMP '2011-02-13 08:02:06.334', 42, 2, 'POISSONS VOLANTS', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528913#0.3771599316686898', 0, TIMESTAMP '2011-02-13 08:02:06.334', 43, 2, 'CREVETTES', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528914#0.5537528089992184', 0, TIMESTAMP '2011-02-13 08:02:06.334', 50, 3, 'DAUPHINS', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528914#0.9271190863638722', 0, TIMESTAMP '2011-02-13 08:02:06.334', 51, 3, 'BALEINE', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528914#0.5862595094439679', 0, TIMESTAMP '2011-02-13 08:02:06.334', 52, 3, 'CACHALOT', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528914#0.6747971019280458', 0, TIMESTAMP '2011-02-13 08:02:06.334', 53, 3, 'GLOBICEPHALES  ( poufs )', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528914#0.35618705510279436', 0, TIMESTAMP '2011-02-13 08:02:06.334', 60, 4, 'REQUIN-BALEINE', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528914#0.8939862830871832', 0, TIMESTAMP '2011-02-13 08:02:06.334', 61, 2, 'REQUIN', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473');
INSERT INTO PUBLIC.FISHINGCONTEXT(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, REDUCEDCODE, LIBELLE, SCHOOLTYPE) VALUES
  ('fr.ird.t3.entities.reference.FishingContext#1297580528914#0.4767953957619824', 0, TIMESTAMP '2011-02-13 08:02:06.334', 62, 6, 'RAIE MANTA', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528914#0.983720665376364', 0, TIMESTAMP '2011-02-13 08:02:06.334', 70, 2, 'POISSON ( sans précision )', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528914#0.7442179136941349', 0, TIMESTAMP '2011-02-13 08:02:06.335', 71, 2, 'ESPADON', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528914#0.550742526767446', 0, TIMESTAMP '2011-02-13 08:02:06.335', 72, 2, 'MARLIN', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528914#0.6817882392404121', 0, TIMESTAMP '2011-02-13 08:02:06.335', 73, 2, 'VOILIER', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528914#0.04631816119470289', 0, TIMESTAMP '2011-02-13 08:02:06.335', 80, 2, 'TORTUE', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528915#0.26968533010247153', 0, TIMESTAMP '2011-02-13 08:02:06.335', 81, 1, 'CHAROGNE', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528915#0.8699307919908305', 0, TIMESTAMP '2011-02-13 08:02:06.335', 90, 7, 'Récupération du code apparence ', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528915#0.24521196503184728', 0, TIMESTAMP '2011-02-13 08:02:06.335', 91, 7, 'BL YF (détecté non travaillé ou non capturé)', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528915#0.3048043737092341', 0, TIMESTAMP '2011-02-13 08:02:06.335', 92, 7, 'BL SJ (détecté non travaillé ou non capturé)', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528915#0.07673799523632685', 0, TIMESTAMP '2011-02-13 08:02:06.335', 93, 7, 'BL MELANGE (détecté non travaillé ou non capturé)', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528915#0.363931332263858', 0, TIMESTAMP '2011-02-13 08:02:06.335', 94, 7, 'BL RAVILS (auxis, thonine) (détecté non travaillé ou non capturé)', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528915#0.7739140954923266', 0, TIMESTAMP '2011-02-13 08:02:06.335', 95, 7, 'YF isolés ou à la pièce (détecté non travaillé ou non capturé)', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528915#0.3903255956452554', 0, TIMESTAMP '2011-02-13 08:02:06.335', 96, 7, 'SJ isolés ou à la pièce (détecté non travaillé ou non capturé)', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636'),
  ('fr.ird.t3.entities.reference.FishingContext#1297580528915#0.552294761825094', 0, TIMESTAMP '2011-02-13 08:02:06.335', 97, 7, 'Espèces indéterminées isolées ou à la pièce (détecté non travaillé ou non capturé)', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636');
INSERT INTO PUBLIC.FISHINGCONTEXT(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, REDUCEDCODE, LIBELLE, SCHOOLTYPE) VALUES
  ('fr.ird.t3.entities.reference.FishingContext#1297580528915#0.2522662586422436', 0, TIMESTAMP '2011-02-13 08:02:06.335', 99, 7, 'Idem 90 - lors de la conversion cvcd (data issues d''1 traitement)', 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636');

UPDATE fishingcontext SET status = TRUE;

INSERT INTO PUBLIC.fishingcontext (topiaid, topiaversion, topiacreatedate, code, reducedcode, libelle, status, schooltype) VALUES 
  ('fr.ird.t3.entities.reference.FishingContext#1455728697973#0.1', 0, '2016-02-17 17:02:00.000', 98, 6, 'Pêche avec un senneur pour partage des captures', TRUE, 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636'),
  ('fr.ird.t3.entities.reference.FishingContext#1455728697973#0.2', 0, '2016-02-17 17:02:00.000', 100, 7, 'Pêche avec un senneur pour partage', TRUE, 'fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636');