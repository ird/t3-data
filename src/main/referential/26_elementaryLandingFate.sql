---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO elementarylandingfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.ElementaryLandingFate#1454009635524#0.1', 0, '2016-01-28 00:00:00.000', 1, 'Castelli', TRUE);
INSERT INTO elementarylandingfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.ElementaryLandingFate#1454009635524#0.2', 0, '2016-01-28 00:00:00.000', 2, 'SCODI', TRUE);
INSERT INTO elementarylandingfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.ElementaryLandingFate#1454009635524#0.3', 0, '2016-01-28 00:00:00.000', 3, 'PFCI', TRUE);
INSERT INTO elementarylandingfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.ElementaryLandingFate#1454009635524#0.4', 0, '2016-01-28 00:00:00.000', 4, 'Cargo', TRUE);
INSERT INTO elementarylandingfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.ElementaryLandingFate#1454009635524#0.5', 0, '2016-01-28 00:00:00.000', 5, 'Conteneur', TRUE);
INSERT INTO elementarylandingfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.ElementaryLandingFate#1454009635524#0.6', 0, '2016-01-28 00:00:00.000', 6, 'SOGEF', TRUE);
INSERT INTO elementarylandingfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.ElementaryLandingFate#1454009635524#0.7', 0, '2016-01-28 00:00:00.000', 7, 'IOT', TRUE);
INSERT INTO elementarylandingfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.ElementaryLandingFate#1454009635524#0.8', 0, '2016-01-28 00:00:00.000', 8, 'PFOI', TRUE);
INSERT INTO elementarylandingfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.ElementaryLandingFate#1454009635524#0.9', 0, '2016-01-28 00:00:00.000', 9, 'SENEMER', TRUE);
INSERT INTO elementarylandingfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.ElementaryLandingFate#1454009635524#0.10', 0, '2016-01-28 00:00:00.000', 10, 'PIONNER FOOD CANNERY LTD', TRUE);
INSERT INTO elementarylandingfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.ElementaryLandingFate#1454009635524#0.11', 0, '2016-01-28 00:00:00.000', 11, 'SOCOFROID', TRUE);
INSERT INTO elementarylandingfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.ElementaryLandingFate#1454009635524#0.12', 0, '2016-01-28 00:00:00.000', 12, 'PFC TEMA', TRUE);
INSERT INTO elementarylandingfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.ElementaryLandingFate#1454009635524#0.13', 0, '2016-01-28 00:00:00.000', 13, 'AIRONE CI', TRUE);
INSERT INTO elementarylandingfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.ElementaryLandingFate#1454009635524#0.14', 0, '2016-01-28 00:00:00.000', 14, 'SCASA', TRUE);
INSERT INTO elementarylandingfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.ElementaryLandingFate#1454009635524#0.15', 0, '2016-01-28 00:00:00.000', 15, 'SOCEF', TRUE);
INSERT INTO elementarylandingfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES('fr.ird.t3.entities.reference.ElementaryLandingFate#1454009635524#0.16', 0, '2016-01-28 00:00:00.000', 999, 'Autre', TRUE);