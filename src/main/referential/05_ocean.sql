---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO PUBLIC.OCEAN(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, LIBELLE) VALUES
  ('fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624', 0, TIMESTAMP '2011-02-13 08:02:06.344', 1, 'Atlantique'),
  ('fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', 0, TIMESTAMP '2011-02-13 08:02:06.353', 2, 'Indien'),
  ('fr.ird.t3.entities.reference.Ocean#1297580528924#0.45013994503441146', 0, TIMESTAMP '2011-02-13 08:02:06.353', 3, 'Pacifique Ouest'),
  ('fr.ird.t3.entities.reference.Ocean#1297580528924#0.8628908745736912', 0, TIMESTAMP '2011-02-13 08:02:06.353', 4, 'Pacifique Est'),
  ('fr.ird.t3.entities.reference.Ocean#1297580528925#0.868563686958073', 0, TIMESTAMP '2011-02-13 08:02:06.353', 5, 'Pacifique ');

UPDATE ocean SET status = TRUE;
