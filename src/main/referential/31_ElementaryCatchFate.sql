---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO elementarycatchfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES ('fr.ird.t3.entities.reference.ElementaryCatchFate#1#1', 0, '2016-02-01 00:00:00.000', 1, 'Landed', TRUE);
INSERT INTO elementarycatchfate(topiaid, topiaversion, topiacreatedate, code, libelle, status) VALUES ('fr.ird.t3.entities.reference.ElementaryCatchFate#1#2', 0, '2016-02-01 00:00:00.000', 2, 'Discarded', TRUE);
