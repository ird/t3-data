---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO PUBLIC.SAMPLETYPE(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, LIBELLE) VALUES
  ('fr.ird.t3.entities.reference.SampleType#1297580528942#0.030344268515999584', 0, TIMESTAMP '2011-02-13 08:02:06.396', 1, 'Débarquement'),
  ('fr.ird.t3.entities.reference.SampleType#1297580528942#0.9108326183761342', 0, TIMESTAMP '2011-02-13 08:02:06.421', 2, 'Observateur'),
  ('fr.ird.t3.entities.reference.SampleType#1297580528942#0.24580396084277178', 0, TIMESTAMP '2011-02-13 08:02:06.421', 3, 'Poisson trié'),
  ('fr.ird.t3.entities.reference.SampleType#1297580528942#0.6208813198796548', 0, TIMESTAMP '2011-02-13 08:02:06.421', 4, 'Types de bancs mélangés'),
  ('fr.ird.t3.entities.reference.SampleType#1297580528942#0.6696636468106755', 0, TIMESTAMP '2011-02-13 08:02:06.421', 9, 'Indéterminé'),
  ('fr.ird.t3.entities.reference.SampleType#1297580528943#0.7725442998319689', 0, TIMESTAMP '2011-02-13 08:02:06.421', 11, 'Débarquement en frais (canneurs)');

UPDATE sampleType SET status = TRUE;
