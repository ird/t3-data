---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO PUBLIC.WEIGHTCATEGORYLANDING(TOPIAID, TOPIAVERSION, CODE, SPECIES, TOPIACREATEDATE, SOVLIBELLE, STARLIBELLE) VALUES
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528897#0.9849989043584604', 0, 1, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204', TIMESTAMP '2011-02-13 08:02:06.279', 'Albacore -10', 'Yellowfin R1 + R2+ R3'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528897#0.37431829932158744', 0, 2, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204', TIMESTAMP '2011-02-13 08:02:06.288', 'Albacore +10', 'GG'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528897#0.5314676874701734', 0, 9, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204', TIMESTAMP '2011-02-13 08:02:06.288', 'Albacore ?', 'Yellowfin ?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528897#0.12396399303506567', 0, 10, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204', TIMESTAMP '2011-02-13 08:02:06.288', 'Albacore frais (canneurs)', 'Albacore frais (canneurs)'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528897#0.9880486795552492', 0, 50, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204', TIMESTAMP '2011-02-13 08:02:06.288', 'Inconnu', 'GG + 13.6 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528897#0.846209974442883', 0, 51, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204', TIMESTAMP '2011-02-13 08:02:06.288', 'Inconnu', 'R1 entre 13.6 et 3.4 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528897#0.7042864703615278', 0, 52, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204', TIMESTAMP '2011-02-13 08:02:06.288', 'Inconnu', 'R2 entre 3.4 et 1.8 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528898#0.49007162713264407', 0, 53, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204', TIMESTAMP '2011-02-13 08:02:06.288', 'Inconnu', 'R3 moins de 1.8 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528898#0.6025938943078754', 0, 70, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204', TIMESTAMP '2011-02-13 08:02:06.288', 'Inconnu', 'GG + 13.6 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528898#0.7173389548457879', 0, 71, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204', TIMESTAMP '2011-02-13 08:02:06.288', 'Inconnu', 'R1 entre 13.6 et 3.4 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528898#0.5739515277681424', 0, 72, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204', TIMESTAMP '2011-02-13 08:02:06.288', 'Inconnu', 'R2 entre 3.4 et 1.8 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528898#0.9007932955815551', 0, 73, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204', TIMESTAMP '2011-02-13 08:02:06.288', 'Inconnu', 'R3 moins de 1.8 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528898#0.9187448106864885', 0, 1, 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', TIMESTAMP '2011-02-13 08:02:06.289', 'Listao - 1.8', 'Skipjack R2 + R3'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528898#0.25771673830083053', 0, 2, 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', TIMESTAMP '2011-02-13 08:02:06.289', 'Listao + 1.8', 'Skipjack R1 + Jumbo'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528898#0.9366187356366772', 0, 3, 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', TIMESTAMP '2011-02-13 08:02:06.289', 'Listao -1.5', 'Skipjack ?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528898#0.45933952568779757', 0, 4, 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', TIMESTAMP '2011-02-13 08:02:06.289', 'Listao 1.5 à 1.8', 'Skipjack ?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528898#0.984474862871156', 0, 9, 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', TIMESTAMP '2011-02-13 08:02:06.29', 'Listao ?', 'Skipjack ?');
INSERT INTO PUBLIC.WEIGHTCATEGORYLANDING(TOPIAID, TOPIAVERSION, CODE, SPECIES, TOPIACREATEDATE, SOVLIBELLE, STARLIBELLE) VALUES
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528898#0.682911685142915', 0, 50, 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', TIMESTAMP '2011-02-13 08:02:06.29', '+3.4 Kg', 'JUMBO + 3.4 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528899#0.7220987469827985', 0, 51, 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', TIMESTAMP '2011-02-13 08:02:06.29', '+1.8 Kg à +3.4 Kg', 'R1 entre 3.4 et 1.8 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528899#0.04293797444920311', 0, 52, 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', TIMESTAMP '2011-02-13 08:02:06.29', '-1.8 Kg', 'R2 entre 1.8 et 1.4 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528899#0.9216790161293088', 0, 53, 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', TIMESTAMP '2011-02-13 08:02:06.29', '-1.5 Kg', 'R3 moins de 1.4 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528899#0.6371301485277248', 0, 70, 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', TIMESTAMP '2011-02-13 08:02:06.29', 'Inconnu', 'JUMBO + 3.4 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528899#0.09257426517648015', 0, 71, 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', TIMESTAMP '2011-02-13 08:02:06.29', 'Inconnu', 'R1 entre 3.4 et 1.8 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528899#0.5717845604984648', 0, 72, 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', TIMESTAMP '2011-02-13 08:02:06.29', 'Inconnu', 'R2 entre 1.8 et 1.4 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528899#0.3686414703255526', 0, 73, 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', TIMESTAMP '2011-02-13 08:02:06.29', 'Inconnu', 'R3 moins de 1.4 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528899#0.55718847508238', 0, 1, 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', TIMESTAMP '2011-02-13 08:02:06.29', 'Patudo - 10', 'Bigeye?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528899#0.1154225150942314', 0, 2, 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', TIMESTAMP '2011-02-13 08:02:06.29', 'Patudo + 10', 'Bigeye?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528899#0.2685200181978007', 0, 3, 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', TIMESTAMP '2011-02-13 08:02:06.29', 'Patudo - 15', 'Bigeye?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528900#0.6722831615934043', 0, 4, 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', TIMESTAMP '2011-02-13 08:02:06.291', 'Patudo + 15', 'Bigeye?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528900#0.19938872094926574', 0, 5, 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', TIMESTAMP '2011-02-13 08:02:06.291', 'Patudo + 35', 'Bigeye?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528900#0.5746266936785298', 0, 9, 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', TIMESTAMP '2011-02-13 08:02:06.291', 'Patudo ?', 'Bigeye ?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528900#0.25713892292707874', 0, 10, 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', TIMESTAMP '2011-02-13 08:02:06.291', 'Patudo frais (canneurs)', 'Patudo frais (canneurs)'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528900#0.6685982018841194', 0, 50, 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', TIMESTAMP '2011-02-13 08:02:06.291', 'Inconnu', 'GG + 13.6 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528900#0.6904127607618373', 0, 51, 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', TIMESTAMP '2011-02-13 08:02:06.291', 'Inconnu', 'R1 entre 13.6 et 3.4 kg');
INSERT INTO PUBLIC.WEIGHTCATEGORYLANDING(TOPIAID, TOPIAVERSION, CODE, SPECIES, TOPIACREATEDATE, SOVLIBELLE, STARLIBELLE) VALUES
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528900#0.2909239953543622', 0, 52, 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', TIMESTAMP '2011-02-13 08:02:06.291', 'Inconnu', 'R2 entre 3.4 et 1.8 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528900#0.9449657799790759', 0, 53, 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', TIMESTAMP '2011-02-13 08:02:06.291', 'Inconnu', 'R3 moins de 1.8 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528900#0.33597810152594676', 0, 70, 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', TIMESTAMP '2011-02-13 08:02:06.291', 'Inconnu', 'GG + 13.6 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528900#0.8237006835612211', 0, 71, 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', TIMESTAMP '2011-02-13 08:02:06.291', 'Inconnu', 'R1 entre 13.6 et 3.4 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528901#0.657751933189818', 0, 72, 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', TIMESTAMP '2011-02-13 08:02:06.291', 'Inconnu', 'R2 entre 3.4 et 1.8 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528901#0.5073081843096476', 0, 73, 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', TIMESTAMP '2011-02-13 08:02:06.291', 'Inconnu', 'R3 moins de 1.8 kg'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528901#0.0836610498990451', 0, 2, 'fr.ird.t3.entities.reference.Species#1297580528889#0.7537605030704515', TIMESTAMP '2011-02-13 08:02:06.291', 'Germon ', 'Albacora'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528901#0.9643811025507996', 0, 9, 'fr.ird.t3.entities.reference.Species#1297580528889#0.7537605030704515', TIMESTAMP '2011-02-13 08:02:06.291', 'Germon ?', 'Abacora ?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528901#0.9682190838497855', 0, 9, 'fr.ird.t3.entities.reference.Species#1297580528889#0.4053316565120936', TIMESTAMP '2011-02-13 08:02:06.291', 'Thonine ', 'undefined'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528901#0.45162453191323304', 0, 9, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5568385538955283', TIMESTAMP '2011-02-13 08:02:06.291', 'Auxide ', 'undefined'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528901#0.9463330829438842', 0, 9, 'fr.ird.t3.entities.reference.Species#1297580528889#0.7797586779656372', TIMESTAMP '2011-02-13 08:02:06.291', 'Requins ', 'undefined'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528901#0.8486727929567192', 0, 9, 'fr.ird.t3.entities.reference.Species#1297580528889#0.8288119155884791', TIMESTAMP '2011-02-13 08:02:06.291', 'Rejets', '?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528901#0.5077396278663612', 0, 1, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5683902688416475', TIMESTAMP '2011-02-13 08:02:06.291', 'Thonidés - 10 Kg', '?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528901#0.676520211264963', 0, 2, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5683902688416475', TIMESTAMP '2011-02-13 08:02:06.291', 'Thonidés +10 Kg', '?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528901#0.4959464977070138', 0, 9, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5683902688416475', TIMESTAMP '2011-02-13 08:02:06.291', 'Thonidés', '?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528902#0.76777812435383', 0, 10, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5683902688416475', TIMESTAMP '2011-02-13 08:02:06.292', 'Albacore et Patudo frais (canneurs)', 'Albacore et Patudo frais (canneurs)'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528902#0.3176362508792657', 0, 50, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5683902688416475', TIMESTAMP '2011-02-13 08:02:06.292', 'SJ YF BE 3.4-10', '?');
INSERT INTO PUBLIC.WEIGHTCATEGORYLANDING(TOPIAID, TOPIAVERSION, CODE, SPECIES, TOPIACREATEDATE, SOVLIBELLE, STARLIBELLE) VALUES
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528902#0.6606118784093948', 0, 51, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5683902688416475', TIMESTAMP '2011-02-13 08:02:06.292', 'SJ YF BE 1.8-3.4', '?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528902#0.8531106782664439', 0, 52, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5683902688416475', TIMESTAMP '2011-02-13 08:02:06.292', 'SJ YF BE 1.5-1.8', '?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528902#0.1116407842625079', 0, 53, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5683902688416475', TIMESTAMP '2011-02-13 08:02:06.292', 'SJ YF BE 1-1.5', '?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528902#0.9969288992557673', 0, 54, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5683902688416475', TIMESTAMP '2011-02-13 08:02:06.292', 'SJ YF BE under 1', '?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528902#0.41727746602759885', 0, 9, 'fr.ird.t3.entities.reference.Species#1297580528890#0.7235429225787087', TIMESTAMP '2011-02-13 08:02:06.292', 'Thonine orientale', '?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528902#0.7829486684431057', 0, 9, 'fr.ird.t3.entities.reference.Species#1297580528890#0.5141201542305377', TIMESTAMP '2011-02-13 08:02:06.292', 'Thon mignon', '?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528902#0.014541584988570166', 0, 9, 'fr.ird.t3.entities.reference.Species#1297580528890#0.15829471459979372', TIMESTAMP '2011-02-13 08:02:06.292', 'Thon à nageoires noires', '?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528902#0.902691806614427', 0, 54, 'fr.ird.t3.entities.reference.Species#1297580528889#0.5520768903332204', TIMESTAMP '2011-02-13 08:02:06.292', 'YFT -3.4 Kg', '?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528902#0.6335347201678889', 0, 54, 'fr.ird.t3.entities.reference.Species#1297580528889#0.21694502845340558', TIMESTAMP '2011-02-13 08:02:06.292', 'SKJ -3.4 Kg', '?'),
  ('fr.ird.t3.entities.reference.WeightCategoryLanding#1297580528903#0.49410286646783097', 0, 54, 'fr.ird.t3.entities.reference.Species#1297580528889#0.44612689800927063', TIMESTAMP '2011-02-13 08:02:06.292', 'BET -3.4 Kg', '?');

UPDATE weightcategorylanding SET status = TRUE;
