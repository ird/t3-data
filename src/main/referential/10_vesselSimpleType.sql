---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO PUBLIC.VESSELSIMPLETYPE(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, LIBELLE) VALUES
  ('fr.ird.t3.entities.reference.VesselSimpleType#1301594352566#0.2312748071638543', 1, TIMESTAMP '2011-03-31 19:59:12.565', 1, 'Senneur'),
  ('fr.ird.t3.entities.reference.VesselSimpleType#1301594352570#0.529807600334312', 1, TIMESTAMP '2011-03-31 19:59:12.57', 2, 'Canneur'),
  ('fr.ird.t3.entities.reference.VesselSimpleType#1301594352571#0.48345536653580057', 1, TIMESTAMP '2011-03-31 19:59:12.57', 3, 'Palangrier'),
  ('fr.ird.t3.entities.reference.VesselSimpleType#1301594352571#0.1012136425628073', 1, TIMESTAMP '2011-03-31 19:59:12.571', 4, 'Autre');

UPDATE vesselsimpletype SET status = TRUE;

INSERT INTO PUBLIC.VESSELSIMPLETYPE(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, LIBELLE, STATUS) VALUES
  ('fr.ird.t3.entities.reference.VesselSimpleType#1457585102290#0.1', 1, TIMESTAMP '2016-03-10 08:00:00.000', 5, 'Mixte', TRUE),
  ('fr.ird.t3.entities.reference.VesselSimpleType#1457585102290#0.2', 1, TIMESTAMP '2016-03-10 08:00:00.000', 6, 'Assistance', TRUE);