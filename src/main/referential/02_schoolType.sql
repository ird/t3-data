---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO PUBLIC.SCHOOLTYPE(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, LIBELLE, LIBELLE4, THRESHOLDNUMBERLEVEL2) VALUES
  ('fr.ird.t3.entities.reference.SchoolType#1297580528908#0.8923578380543445', 0, TIMESTAMP '2011-02-13 08:02:06.309', 1, 'Banc sous objet', 'BO', 4500),
  ('fr.ird.t3.entities.reference.SchoolType#1297580528908#0.633774803291473', 0, TIMESTAMP '2011-02-13 08:02:06.319', 2, 'Banc libre', 'BL', 3000),
  ('fr.ird.t3.entities.reference.SchoolType#1297580528908#0.9747685881931636', 0, TIMESTAMP '2011-02-13 08:02:06.319', 3, 'Indéterminé', 'IND', NULL);
UPDATE schooltype SET status = TRUE;
