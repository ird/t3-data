---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO PUBLIC.VESSELTYPE(TOPIAID, TOPIAVERSION, TOPIACREATEDATE, CODE, LIBELLE, VESSELSIMPLETYPE) VALUES
  ('fr.ird.t3.entities.reference.VesselType#1297580528734#0.7035279136355298', 1, TIMESTAMP '2011-02-13 08:02:05.665', 1, 'Canne / glacier', 'fr.ird.t3.entities.reference.VesselSimpleType#1301594352570#0.529807600334312'),
  ('fr.ird.t3.entities.reference.VesselType#1297580528734#0.048529231285481034', 1, TIMESTAMP '2011-02-13 08:02:05.681', 2, 'Canne / congélateur', 'fr.ird.t3.entities.reference.VesselSimpleType#1301594352570#0.529807600334312'),
  ('fr.ird.t3.entities.reference.VesselType#1297580528735#0.2819883009180477', 1, TIMESTAMP '2011-02-13 08:02:05.681', 4, 'Senneur avec appât', 'fr.ird.t3.entities.reference.VesselSimpleType#1301594352566#0.2312748071638543'),
  ('fr.ird.t3.entities.reference.VesselType#1297580528735#0.13369549739869058', 1, TIMESTAMP '2011-02-13 08:02:05.681', 5, 'Senneur sans appât', 'fr.ird.t3.entities.reference.VesselSimpleType#1301594352566#0.2312748071638543'),
  ('fr.ird.t3.entities.reference.VesselType#1297580528735#0.6353515731255521', 1, TIMESTAMP '2011-02-13 08:02:05.682', 6, 'Grand senneur', 'fr.ird.t3.entities.reference.VesselSimpleType#1301594352566#0.2312748071638543'),
  ('fr.ird.t3.entities.reference.VesselType#1297580528735#0.9403907011608672', 1, TIMESTAMP '2011-02-13 08:02:05.682', 7, 'Palangrier', 'fr.ird.t3.entities.reference.VesselSimpleType#1301594352571#0.48345536653580057'),
  ('fr.ird.t3.entities.reference.VesselType#1297580528735#0.38004555791991557', 1, TIMESTAMP '2011-02-13 08:02:05.682', 8, 'Bateau Mère', 'fr.ird.t3.entities.reference.VesselSimpleType#1301594352571#0.1012136425628073'),
  ('fr.ird.t3.entities.reference.VesselType#1297580528735#0.4214855823429522', 1, TIMESTAMP '2011-02-13 08:02:05.682', 9, 'Bateau Usine', 'fr.ird.t3.entities.reference.VesselSimpleType#1301594352571#0.1012136425628073'),
  ('fr.ird.t3.entities.reference.VesselType#1297580528735#0.9767149863653742', 1, TIMESTAMP '2011-02-13 08:02:05.682', 10, 'Supply', 'fr.ird.t3.entities.reference.VesselSimpleType#1457585102290#0.2'),
  ('fr.ird.t3.entities.reference.VesselType#1297580528735#0.6167953512531488', 1, TIMESTAMP '2011-02-13 08:02:05.682', 11, 'Cargo', 'fr.ird.t3.entities.reference.VesselSimpleType#1301594352571#0.1012136425628073'),
  ('fr.ird.t3.entities.reference.VesselType#1297580528735#0.914816309303257', 1, TIMESTAMP '2011-02-13 08:02:05.682', 12, 'Canneur / Supply', 'fr.ird.t3.entities.reference.VesselSimpleType#1457585102290#0.2'),
  ('fr.ird.t3.entities.reference.VesselType#1297580528735#0.7440760831314138', 1, TIMESTAMP '2011-02-13 08:02:05.682', 99, 'Inconnu', 'fr.ird.t3.entities.reference.VesselSimpleType#1301594352571#0.1012136425628073'),
  ('fr.ird.t3.entities.reference.VesselType#1297580528734#0.99230421278849', 2, TIMESTAMP '2011-02-13 08:02:05.681', 3, 'Mixte', 'fr.ird.t3.entities.reference.VesselSimpleType#1457585102290#0.1');

UPDATE vesselType SET status = TRUE;