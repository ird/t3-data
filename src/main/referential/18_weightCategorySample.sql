---
-- #%L
-- T3 :: Data
-- %%
-- Copyright (C) 2016 IRD, Codelutin, Tony Chemit
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
-- #L%
---
INSERT INTO WeightCategorySample(topiaid, topiaversion, topiacreatedate, min, max, ocean, libelle) VALUES ('fr.ird.t3.entities.reference.WeightCategorySample#1297580528908#0.01', 0, TIMESTAMP '2016-01-25 19:04:00.01', 0,  10,   'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', '-10Kg');
INSERT INTO WeightCategorySample(topiaid, topiaversion, topiacreatedate, min, max, ocean, libelle) VALUES ('fr.ird.t3.entities.reference.WeightCategorySample#1297580528908#0.02', 0, TIMESTAMP '2016-01-25 19:04:00.02', 10, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.02462443299831396', '+10Kg');
INSERT INTO WeightCategorySample(topiaid, topiaversion, topiacreatedate, min, max, ocean, libelle) VALUES ('fr.ird.t3.entities.reference.WeightCategorySample#1297580528908#0.03', 0, TIMESTAMP '2016-01-25 19:04:00.03', 0,  10,   'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624',  '-10Kg');
INSERT INTO WeightCategorySample(topiaid, topiaversion, topiacreatedate, min, max, ocean, libelle) VALUES ('fr.ird.t3.entities.reference.WeightCategorySample#1297580528908#0.04', 0, TIMESTAMP '2016-01-25 19:04:00.04', 10, NULL, 'fr.ird.t3.entities.reference.Ocean#1297580528924#0.4917298410119624',  '+10Kg');

UPDATE WeightCategorySample SET status = TRUE;
