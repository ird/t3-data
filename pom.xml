<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  T3 :: Data
  %%
  Copyright (C) 2016 IRD, Codelutin, Tony Chemit
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses />.
  #L%
  -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>org.nuiton</groupId>
    <artifactId>codelutinpom</artifactId>
    <version>6</version>
  </parent>

  <groupId>fr.ird</groupId>
  <artifactId>t3-data</artifactId>
  <version>1.2-SNAPSHOT</version>

  <name>T3 :: Data</name>

  <description>Data of T3+ project</description>
  <inceptionYear>2016</inceptionYear>
  <url>http://t3.codelutin.com</url>

  <organization>
    <name>IRD</name>
    <url>http://www.ird.fr/</url>
  </organization>

  <licenses>
    <license>
      <name>Affero General Public License (AGPL)</name>
      <url>http://www.gnu.org/licenses/agpl.txt</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <developers>
    <developer>
      <id>chemit</id>
      <name>Tony Chemit</name>
      <email>chemit at codelutin dot com</email>
      <organization>CodeLutin</organization>
      <organizationUrl>http://codelutin.com</organizationUrl>
      <roles>
        <role>lead</role>
        <role>developer</role>
      </roles>
      <timezone>Europe/Paris</timezone>
    </developer>

  </developers>

  <scm>
    <url>https://gitlab.nuiton.org/codelutin/t3-data</url>
    <connection>scm:git:git@gitlab.nuiton.org:codelutin/t3-data.git</connection>
    <developerConnection>scm:git:git@gitlab.nuiton.org:codelutin/t3-data.git</developerConnection>
  </scm>

  <distributionManagement>
    <site>
      <id>${site.server}</id>
      <url>${site.url}</url>
    </site>
  </distributionManagement>

  <packaging>pom</packaging>

  <properties>

    <!--  Super pom properties  -->
    <projectId>t3</projectId>
    <redmine.versionId>t3-data-${project.version}</redmine.versionId>

    <!-- license header configuration -->
    <license.organizationName>
      IRD, Codelutin, Tony Chemit
    </license.organizationName>
    <license.licenseName>agpl_v3</license.licenseName>

    <!-- Post Release configuration -->
    <skipPostRelease>false</skipPostRelease>
    <!-- Do deploy artifacts -->
    <maven.deploy.skip>false</maven.deploy.skip>
    <!-- Post Release configuration -->
    <skipPostRelease>false</skipPostRelease>

  </properties>


  <repositories>
    <repository>
      <id>codelutin-public</id>
      <name>Codelutin public repository 4All</name>
      <releases>
        <enabled>true</enabled>
      </releases>
      <snapshots>
        <enabled>true</enabled>
      </snapshots>
      <url>http://nexus.nuiton.org/nexus/content/groups/public</url>
    </repository>
  </repositories>

  <build>

    <extensions>
      <!-- Enabling the use of scpexe with maven 3.0 -->
      <extension>
        <groupId>org.apache.maven.wagon</groupId>
        <artifactId>wagon-ssh-external</artifactId>
        <version>2.2</version>
      </extension>
    </extensions>

    <plugins>

      <plugin>
        <artifactId>maven-assembly-plugin</artifactId>
        <executions>
          <execution>
            <phase>package</phase>
            <goals>
              <goal>single</goal>
            </goals>
            <configuration>
              <descriptors>
                <descriptor>src/main/assembly/ddl.xml</descriptor>
                <descriptor>src/main/assembly/referential.xml</descriptor>
                <descriptor>src/main/assembly/zones.xml</descriptor>
              </descriptors>
            </configuration>
          </execution>
        </executions>
      </plugin>

    </plugins>

  </build>

</project>
